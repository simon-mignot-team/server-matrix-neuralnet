import numpy as np

from NeuralNetwork.NNComponentShaper import NNComponentShaper as nncs


def test_flatten_weights():
    to_flatten = [np.array([[1., 2.], [3., 4.], [5., 6.], [7., 8.], [9., 10.]]),
                  np.array([[1., 2., 3., 4., 5.], [6., 7., 8., 9., 10.], [11., 12., 13., 14., 15.]])]
    expected = [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.,
                1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15.]
    assert nncs.flatten_weights(to_flatten) == expected


def test_shape_weights():
    to_shape = [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.,
                1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15.]
    expected = [np.array([[1., 2.], [3., 4.], [5., 6.], [7., 8.], [9., 10.]]),
                np.array([[1., 2., 3., 4., 5.], [6., 7., 8., 9., 10.], [11., 12., 13., 14., 15.]])]
    shapes = [(5, 2), (3, 5)]

    result = nncs.shape_weights(to_shape, shapes)

    np.testing.assert_almost_equal(result[0], expected[0])
    np.testing.assert_almost_equal(result[1], expected[1])


def test_flatten_outputs():
    to_flatten = np.array([[1.], [2.], [3.], [4.], [5.]])
    expected = [1., 2., 3., 4., 5.]
    assert nncs.flatten_outputs(to_flatten) == expected


def test_shape_inputs():
    to_shape = [1., 2., 3., 4., 5.]
    expected = np.array([[1.], [2.], [3.], [4.], [5.]])
    result = nncs.shape_inputs(to_shape)
    np.testing.assert_almost_equal(result, expected)

