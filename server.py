from json import dumps

from bottle import get, post, put, delete, run, response, request, hook, route

from NeuralNetwork import NeuralNet
from Server.NeuralNetworksHandler import NeuralNetworksHandler


NotImplementedJSON = '{"error": "NotImplemented"}'
nnh = NeuralNetworksHandler.get_instance()


@hook('before_request')
def set_default():
    response.set_header("Content-Type", "application/json")


@hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


@route("/<url:re:.+>", method=['OPTIONS'])
def options(url):
    pass


# 200 : OK
@get('/neural-net')
def get_nns():
    return dumps(nnh.get_ids_list())


# 201 : OK
# 400 : OK
@post('/neural-net')
def create_nn():
    body = request.json
    result = []
    for i in range(int(body["quantity"])):
        identifier = nnh.add_new_nn(body["neuronsPerLayer"], body["layersFunctions"], NeuralNet.BIAS_EACHLAYER)
        if identifier is None:
            response.status = 400
            return
        result.append(identifier)
    response.status = 201
    return dumps(result)


# 200 : KO
# 404 : KO
@delete('/neural-net')
def delete_nns():
    body = request.json
    result = []
    for nn_id in body:
        if not nnh.nn_exists(nn_id):
            response.status = 404
            return
    for nn_id in body:
        nnh.delete_nn(nn_id)
    response.status = 200
    return


# 200 : OK
# 400 : OK
# 404 : OK
@post('/neural-net/activate')
def activate_nns():
    body = request.json
    result = {}
    for nn in body:
        if not nnh.nn_exists(nn["id"]):
            response.status = 404
            return
        temp = nnh.activate_nn(nn["id"], nn["in"])
        if temp is None:
            response.status = 400
            return
        result[nn["id"]] = temp
    response.status = 200
    return result


# 200 : OK
# 404 : OK
@post('/neural-net/weights')
def get_nns_weights():
    body = request.json
    result = {}
    for nn_id in body:
        if not nnh.nn_exists(nn_id):
            response.status = 404
            return
        result[nn_id] = nnh.get_nn_weights(nn_id)
    response.status = 200
    return result


# 200 : KO
# 400 : KO
# 404 : KO
@put('/neural-net/weights')
def set_nns_weights():
    body = request.json
    for item in body:
        if not nnh.nn_exists(item["id"]):
            response.status = 404
            return
        if not nnh.nn_compare_weights_length(item["id"], len(item["ws"])):
            response.status = 400
            return
    for item in body:
        nnh.set_nn_weights(item["id"], item["ws"])
    response.status = 200
    return


run(host='localhost', port=3000, debug=True)
