import numpy as np

from NeuralNetwork import NeuralNet
from Server.NeuralNetworksHandler import NeuralNetworksHandler as Nnh

np.set_printoptions(precision=2, linewidth=222, suppress=True)


nnh = Nnh.get_instance()

l = [2, 5, 3]
f = [sigmoid, relu]
b = NeuralNet.BIAS_NOBIAS

inputs = np.matrix([[.5], [0.]])

i = nnh.add_new_nn(l, f, b)

print(nnh.neural_networks[i].weights_shapes)
print(nnh.neural_networks[i].layers_neurons_count)

#o = np.array(nnh.activate_nn(i, inputs))
#o.shape = (o.shape[0],)

print(i)
#print(o)

#
# # l2 = [2, 10000, 1000, 5000, 500, 5]
#
# b = NeuralNet.BIAS_NOBIAS
#
# nn_handler = NeuralNetworksHandler()
#
# nn_id = nn_handler.add_new_nn(l, f, b)
# # nn_handler.add_new_nn(l2, f, b)
#
# nn = nn_handler.neural_networks[nn_id]
#
# print("out")
# print(nn.activate(inputs))
#
# dna = nn.layers_weights
# print("dna")
# print(dna)

# r = nn_handler.neural_networks[nn_id].activate(inputs)
# print(r)
# print(r.dtype)


# while i < 1:
#     inputs = np.matrix([[.5], [0]])
#
#     l = [2, 10000, 1000, 5000, 500, 5]
#     f = [sigmoid, sigmoid, sigmoid, sigmoid, linear]
#     b = NeuralNet.BIAS_NOBIAS
#     connections = 0
#     for j in range(len(l) - 1):
#         connections += l[j] * l[j + 1]
#
#     print("Neural net " + str(i))
#     print("Connection : " + "{:,}".format(connections))
#
#     start = timer()
#     nn = NeuralNet.NeuralNet(l, f, b)
#     end = timer()
#     time = end - start
#     print("Time : " + str(time) + "s")
#     print("Time per connection : " + str(time / connections) + "s")
#     output = nn.activate(inputs)
#     if output[0] == 0 and output[1] == 0:
#         continue
#     print("DNA")
#     print(nn.get_dna())
#     print("Output")
#     print(output)
#     print()
#     i += 1
