import numpy as np


class NNComponentShaper:
    # list[array(li1, li2)] -> list
    @staticmethod
    def flatten_weights(w):
        result = []
        for i in range(len(w)):
            result.extend(np.array(w[i]).flatten().tolist())
        return np.array(result).flatten().tolist()

    # list -> list[array(li1, li2)]
    @staticmethod
    def shape_weights(w, s):
        result = []
        for i in range(len(s)):
            # Initialize some values
            current_dna_amount = s[i][0] * s[i][1]

            # Implant and reshape dna for current layer
            current_dna = np.array(w[:current_dna_amount])
            current_dna.shape = s[i]
            result.append(current_dna)

            # Remove utilized dna
            w = w[current_dna_amount:]
        return result

    # array(x, 1) -> list
    @staticmethod
    def flatten_outputs(outs):
        outs.shape = (outs.shape[0],)
        return outs.tolist()

    # list -> array(x, 1)
    @staticmethod
    def shape_inputs(ins):
        result = np.array(ins)
        result.shape = (result.shape[0], 1)
        return result

    @staticmethod
    def calculate_weights_length(shapes):
        result = 0
        for i in shapes:
            result += i[0] * i[1]
        return result
