import math
import numpy as np


class ActivationsFunctions:
    __initialized = False
    __functions = {}

    @classmethod
    def init(cls):
        cls.__functions["sigmoid"] = np.vectorize(cls.fn_sigmoid)
        cls.__functions["relu"] = np.vectorize(cls.fn_relu)
        cls.__functions["linear"] = np.vectorize(cls.fn_linear)

    @classmethod
    def from_string(cls, fns):
        if not cls.__initialized:
            cls.init()
        result = []
        for f in fns:
            if f in cls.__functions:
                result.append(cls.__functions[f])
            else:
                return None
        return result

    @staticmethod
    def fn_sigmoid(x):
        return 1. / (1. + math.exp(-x))

    @staticmethod
    def fn_relu(x):
        if x > 0:
            return x
        return 0.

    @staticmethod
    def fn_linear(x):
        return x
