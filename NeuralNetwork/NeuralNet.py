import numpy as np
from random import *


BIAS_FIRSTLAYER = 0
BIAS_EACHLAYER = 1
BIAS_NOBIAS = 2


DEBUG_PRINTING = False


def dprint(v):
    if DEBUG_PRINTING:
        print(v)


class NeuralNet:
    def __init__(self, layers_neurons_count, layers_functions, bias):
        NeuralNet.check_parameters(layers_neurons_count, layers_functions)
        self.layers_functions = layers_functions
        self.bias = bias
        self.layers_neurons_count = layers_neurons_count
        self.weights_shapes = []
        self.layers_weights = self.generate_layers_weights()

    @staticmethod
    def check_parameters(lnc, lf):
        if (len(lnc) - 1) != len(lf):
            raise ValueError

    def activate(self, inputs):
        for i in range(len(self.layers_weights)):
            w = self.layers_weights[i]
            dprint("Layer " + str(i) + " : " + self.layers_functions[i].pyfunc.__name__)
            if self.bias == BIAS_EACHLAYER or (self.bias == BIAS_FIRSTLAYER and i == 0):
                w = NeuralNet.add_bias_weights(w)
                inputs = NeuralNet.add_bias_values(inputs)
            dprint("Weights")
            dprint(w)
            dprint("Input")
            dprint(inputs)
            inputs = self.layers_functions[i](w.dot(inputs))
            dprint("Output")
            dprint(inputs)
            dprint("")
        return inputs

    def generate_layers_weights(self):
        result = []
        for i in range(len(self.layers_neurons_count) - 1):
            first = self.layers_neurons_count[i]
            second = self.layers_neurons_count[i + 1]
            shape = (second, first)
            self.weights_shapes.append(shape)
            result.append(NeuralNet.generate_random_weights(shape))
        return result

    @staticmethod
    def generate_random_weights(shape):
        return np.random.rand(shape[0], shape[1]) * 2. - 1.

    @staticmethod
    def generate_random_discrete_weights(shape):
        result = []
        r = [-1., -.75, -.5, -.25, 0., .25, .5, .75, 1.]
        for i in range(shape[0] * shape[1]):
            rid = int(random() * len(r))
            result.append(r[rid])
            # result.append(i)
        result = np.array(result)
        result.shape = shape
        return result

    @staticmethod
    def add_bias_weights(weights):
        ones = np.ones((weights.shape[0], weights.shape[1] + 1))
        ones.dtype = 'float64'
        ones[:, :-1] = weights
        return ones

    @staticmethod
    def add_bias_values(values):
        return np.vstack([values, [1.]])
