import time

from NeuralNetwork.NeuralNet import NeuralNet
from NeuralNetwork.NNComponentShaper import NNComponentShaper
from NeuralNetwork.ActivationFunctions import ActivationsFunctions


class NeuralNetworksHandler:
    __shared_instance = None

    def __init__(self):
        self.neural_networks = {}

    @classmethod
    def get_instance(cls):
        if cls.__shared_instance is None:
            cls.__shared_instance = NeuralNetworksHandler()
        return cls.__shared_instance

    # CREATE
    # Return:
    #   Neural net id if parameters length are correct
    #   None if incorrect
    def add_new_nn(self, layers_neurons_count, layers_functions, bias):
        layers_functions = ActivationsFunctions.from_string(layers_functions)
        try:
            nn = NeuralNet(layers_neurons_count, layers_functions, bias)
        except ValueError:
            return None
        nn_id = self.generate_nn_id(layers_neurons_count, layers_functions, bias)
        self.neural_networks[nn_id] = nn
        return nn_id

    # DELETE
    # Return:
    #   True if deleted
    #   False if id does not exists
    def delete_nn(self, identifier):
        try:
            del self.neural_networks[identifier]
            return True
        except KeyError:
            return False

    # ACTIVATE
    # Return:
    #   None if id does not exists
    #   Array of outputs number otherwise
    def activate_nn(self, identifier, inputs):
        if not self.nn_exists(identifier):
            return None
        inputs = NNComponentShaper.shape_inputs(inputs)
        try:
            outputs = self.neural_networks[identifier].activate(inputs)
        except ValueError:
            return None
        return NNComponentShaper.flatten_outputs(outputs)

    # GET_WEIGHTS
    # Return:
    #   None if id does not exists
    #   Array of flattened weights number otherwise
    def get_nn_weights(self, identifier):
        if not self.nn_exists(identifier):
            return None
        return NNComponentShaper.flatten_weights(self.neural_networks[identifier].layers_weights)

    # SET_WEIGHTS
    # Return:
    #   False if id does not exists
    #   True otherwise
    def set_nn_weights(self, identifier, weights):
        if not self.nn_exists(identifier):
            return None
        s = self.neural_networks[identifier].weights_shapes
        self.neural_networks[identifier].layers_weights = NNComponentShaper.shape_weights(weights, s)
        return True

    def nn_exists(self, identifier):
        return identifier in self.neural_networks

    # GET
    # Return:
    #   List of all neural net IDs
    def get_ids_list(self):
        result = []
        for key, value in self.neural_networks.items():
            result.append(key)
        return result

    def nn_compare_weights_length(self, identifier, length):
        if not self.nn_exists(identifier):
            return None
        return NNComponentShaper.calculate_weights_length(self.neural_networks[identifier].weights_shapes) == length

    @staticmethod
    def check_creation_parameters_length(lnc_len, lf_len):
        return lf_len == (lnc_len - 1)

    @staticmethod
    def generate_nn_id(layers_neurons_count, layers_functions, bias):
        result = "["
        for i in layers_neurons_count:
            result += str(i) + "-"
        result = result[:-1]
        result += "."
        for f in layers_functions:
            result += f.pyfunc.__name__[3:6] + "-"
        result = result[:-1]
        result += ".{" + str(bias) + "}].[" + str(time.time()) + "]"
        return result
